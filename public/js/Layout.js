//页面是否可以选择
var PAGE_CANSELECT = true;

//弹出提示框
function ShowMessageBox(content) {
  $("#MsgboxContent").text(content);
  $('#msgbox').modal();
}
var CookieManager = {};
//-------------------------
//Cookies管理
//-------------------------
//设置cookie
CookieManager.setCookie = (c_name, value, expiredays) => {
  var exdate = new Date();　　　　
  exdate.setDate(exdate.getDate() + expiredays);　　　　
  document.cookie = c_name + "=" + escape(value) + ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString());
}

//获取cookie、
function getCookie(name) {
  var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
  if (arr = document.cookie.match(reg))
    return unescape((arr[2]));
  else
    return null;
}
CookieManager.getCookie = getCookie;
//删除cookie
CookieManager.delCookie = (name) => {
  var exp = new Date();
  exp.setTime(exp.getTime() - 1);
  var cval = getCookie(name);
  if (cval != null)
    document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
}