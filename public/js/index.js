var TIMES = [CPUCOUNT];
var NICES = [CPUCOUNT];
var SYSS = [CPUCOUNT];
var USERS = [CPUCOUNT];
var IDLES = [CPUCOUNT];
var IRQS = [CPUCOUNT];
var OLDNICE = [CPUCOUNT];
var OLDSYS = [CPUCOUNT];
var OLDUSER = [CPUCOUNT];
var OLDIDLE = [CPUCOUNT];
var OLDIRQ = [CPUCOUNT];
for (var i = 0; i < CPUCOUNT; i++) {
	TIMES[i] = [];
	NICES[i] = [];
	SYSS[i] = [];
	USERS[i] = [];
	IDLES[i] = [];
	IRQS[i] = [];
	OLDNICE[i] = 0;
	OLDSYS[i] = 0;
	OLDUSER[i] = 0;
	OLDIDLE[i] = 0;
	OLDIRQ[i] = 0;
}

// 指定图表的配置项和数据
var CPU_OPTION = {
	title: {
		text: ''
	},
	tooltip: {
		trigger: 'axis'
	},
	legend: {
		data: ['用户', '系统', '空闲', '中断', '良好']
	},
	grid: {
		left: '3%',
		right: '4%',
		bottom: '3%',
		containLabel: true
	},
	toolbox: {
		feature: {
			saveAsImage: {}
		}
	},
	xAxis: {
		type: 'category',
		boundaryGap: false,
		data: []
	},
	yAxis: {
		type: 'value',
		max: '100'
	},
	series: [{
			name: '用户',
			type: 'line',
			stack: '总量',
			data: []
		},
		{
			name: '系统',
			type: 'line',
			stack: '总量',
			data: []
		},
		{
			name: '空闲',
			type: 'line',
			stack: '总量',
			data: []
		},
		{
			name: '中断',
			type: 'line',
			stack: '总量',
			data: []
		},
		{
			name: '良好',
			type: 'line',
			stack: '总量',
			data: []
		}
	]
};

var MEM_OPTION = {
	title: {
		text: '内存使用率'
	},
	tooltip: {
		trigger: 'axis',
		axisPointer: {
			type: 'cross',
			label: {
				backgroundColor: '#6a7985'
			}
		}
	},
	legend: {
		data: ['使用']
	},
	toolbox: {
		feature: {
			saveAsImage: {}
		}
	},
	grid: {
		left: '3%',
		right: '4%',
		bottom: '3%',
		containLabel: false
	},
	xAxis: [{
		type: 'category',
		boundaryGap: false,
		data: []
	}],
	yAxis: [{
		type: 'value',
		max: 100
	}],
	series: [{
			name: '使用',
			type: 'line',
			stack: '总量',
			areaStyle: {
				normal: {}
			},
			data: []
		}
		/*,
		{
			name: '空闲',
			type: 'line',
			stack: '总量',
			areaStyle: {
				normal: {}
			},
			data: []
		}
		// */
	]
};

var CPUIMG = [CPUCOUNT];
var MEMIMG = undefined;
var PROGRESS1=undefined;
var MEMINFO=undefined;
var OLDTable = 0;
//切换table
function SwitchTable(index) {
	$("#switch_" + OLDTable).removeClass('selected');
	$("#switch_" + index).addClass('selected');
	$("#table_" + OLDTable).hide(500, () => {
		$("#table_" + index).show(500);
	});

	OLDTable = index;
}

function Takeinfo() {
	$.get("api/TaskManager/", function (result) {
		RefreshImage();
		//更新CPU数据
		for (var i = 0; i < CPUCOUNT; i++) {
			TIMES[i].push(new Date().toLocaleTimeString());
			let nice = result.cpu.info[i].times.nice - OLDNICE[i];
			let sys = result.cpu.info[i].times.sys - OLDSYS[i];
			let user = result.cpu.info[i].times.user - OLDUSER[i];
			let idle = result.cpu.info[i].times.idle - OLDIDLE[i];
			let irq = result.cpu.info[i].times.irq - OLDIRQ[i];
			OLDNICE[i] = result.cpu.info[i].times.nice;
			OLDSYS[i] = result.cpu.info[i].times.sys;
			OLDUSER[i] = result.cpu.info[i].times.user;
			OLDIDLE[i] = result.cpu.info[i].times.idle;
			OLDIRQ[i] = result.cpu.info[i].times.irq;
			let all = nice + sys + user + idle + irq;
			NICES[i].push((nice / all * 100).toFixed(0));
			SYSS[i].push((sys / all * 100).toFixed(0));
			USERS[i].push((user / all * 100).toFixed(0));
			IDLES[i].push((idle / all * 100).toFixed(0));
			IRQS[i].push((irq / all * 100).toFixed(0));
			if (TIMES[i].length > 60) {
				TIMES[i].shift();
				NICES[i].shift();
				SYSS[i].shift();
				USERS[i].shift();
				IDLES[i].shift();
				IRQS[i].shift();
			}

			CPU_OPTION.title.text = "CPU" + i;
			CPU_OPTION.xAxis.data = TIMES[i];
			CPU_OPTION.series[0].data = USERS[i];
			CPU_OPTION.series[1].data = SYSS[i];
			CPU_OPTION.series[2].data = IDLES[i];
			CPU_OPTION.series[3].data = IRQS[i];
			CPU_OPTION.series[4].data = NICES[i];
			CPUIMG[i].setOption(CPU_OPTION);
		}

		//更新内存数据
		MEM_OPTION.xAxis[0].data.push(new Date().toLocaleTimeString())
		let ratio=((result.memory.used/result.memory.total)*100).toFixed();
		MEM_OPTION.series[0].data.push(ratio);
		if (MEM_OPTION.xAxis[0].data.length > 60) {
			MEM_OPTION.xAxis[0].data.shift();
			MEM_OPTION.series[0].data.shift();
		}
		MEMIMG.setOption(MEM_OPTION);
		PROGRESS1.ratio=ratio;
		MEMINFO.total=result.memory.total;
		MEMINFO.used=result.memory.used;
		if(result.memory.total>1024*1024*1024)
		{
			MEMINFO.format_total=(result.memory.total/(1024*1024*1024)).toFixed(1)+"GB"
		}
		else
		{
			MEMINFO.format_total=(result.memory.total/(1024*1024)).toFixed()+"MB"
		}
		if(result.memory.used>1024*1024*1024)
		{
			MEMINFO.format_used=(result.memory.used/(1024*1024*1024)).toFixed(1)+"GB"
		}
		else
		{
			MEMINFO.format_used=(result.memory.used/(1024*1024)).toFixed()+"MB"
		}
	});
}

function RefreshImage()
{
	MEMIMG.resize();
	for (var i = 0; i < CPUCOUNT; i++) {
		CPUIMG[i].resize();
	}
}

//界面加载完毕后执行
$(function () {
	PROGRESS1=new Vue({
		el: '#progress1',
		data: {
			ratio: 0,
		}
	});
	MEMINFO=new Vue({
		el: '#mem_info',
		data: {
			total: 0,
			used: 0,
			format_total:"",
			format_used:""
		}
	});
	setInterval(Takeinfo, 1000);
	MEMIMG = echarts.init(document.getElementById('mem-img'.toString()));
	// 使用刚指定的配置项和数据显示图表。
	MEMIMG.setOption(MEM_OPTION);
	for (var i = 0; i < CPUCOUNT; i++) {
		CPUIMG[i] = echarts.init(document.getElementById('cpu-img-' + i.toString()));
		CPU_OPTION.title.text = "CPU" + i;
		// 使用刚指定的配置项和数据显示图表。
		CPUIMG[i].setOption(CPU_OPTION);
	}
	$("#table_1").hide();
	$("#table_2").hide();
});