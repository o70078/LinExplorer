/**
 * vue对象_目录盘
 */
var DISH = null;

/**
 * vue对象_组件大全
 */
var VUE = null;

/**
 * vue对象_地址栏
 */
var ADDRESSBAR = null;
/**
 * vue对象_菜单
 */
var MENU = null;
/**
 * 当前路径
 */
var CURRENTPATH = "/";
/**
 * 上传中的文件
 */
var UPLOADINGEILES = [];
/**
 * 选中的对象
 */
var CHOOSEITEMS = [];
/**
 * 剪切或者复制的文件
 */
var CUTORCOPYITEMS = [];
/**
 * 粘贴模式 -1:不可粘贴 0:剪切 1:复制
 */
var PASTEMODE = -1;
/**
 * 当前路径是否可写
 */
var CANWRITE = false;
/**
 * 鼠标点击时的位置
 */
var DOWNPOSITION = {
	X: 0,
	Y: 0
};
/**
 * 鼠标当前位置
 */
var CURRENTPOSITION = {
	X: 0,
	Y: 0
};

/**
 * 上传事件
 * @param {object} options 上传参数    
 * @param {object} file 上传文件  
 */
function uploadEvent(options, file) {
	let formData = new FormData();
	let input_file = document.createElement('input');
	input_file.setAttribute('type', 'file');
	input_file.setAttribute('name', 'file');
	formData.append('file', file);

	let input_path = document.createElement('input');
	input_path.setAttribute('type', 'text');
	input_path.setAttribute('name', 'path');
	formData.append('path', CURRENTPATH);
	let listitem = {
		File: file,
		progress: 0,
		total: 0,
		loaded: 0
	};
	UPLOADINGEILES.push(listitem);
	//console.log(UPLOADINGEILES);
	try {
		let xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function () {
			if (xhr.readyState === 4 && xhr.status === 200) {
				for (let i = UPLOADINGEILES.length - 1; i >= 0; i--) {
					if (UPLOADINGEILES[i].File == file) UPLOADINGEILES.splice(i, 1);
				}
				options.success(JSON.parse(xhr.responseText)); //上传成功回调
				ADDRESSBAR.vprogress = UPLOADINGEILES.length > 0;
			}
		}
		xhr.upload.onprogress = function (evt) {
			listitem.loaded = evt.loaded;
			listitem.total = evt.total;
			listitem.progress = Math.floor(100 * evt.loaded / evt.total);
			options.progress(listitem.Progress); //进度变化回调
			//统计并显示总进度:
			ADDRESSBAR.vprogress = UPLOADINGEILES.length > 0;
			let totals = 0;
			let loadeds = 0;
			for (let i = 0, f; f = UPLOADINGEILES[i]; i++) {
				if (!f) break;
				totals += f.total;
				loadeds += f.loaded;
			}
			ADDRESSBAR.progress = Math.floor(100 * loadeds / totals);
		}
		xhr.open('post', '/api/FileManager/UploadFile');
		xhr.send(formData); //开始上传
	} catch (err) {
		for (let i = UPLOADINGEILES.length - 1; i >= 0; i--) {
			if (UPLOADINGEILES[i].File == file) UPLOADINGEILES.splice(i, 1);
		}
		ADDRESSBAR.vprogress = UPLOADINGEILES.length > 0;
		options.fail(err); //上传失败回调
	}
}

/**
 * 格式化路径
 * @param {string} url 
 */
function FormatPath(url) {
	let Path = url;
	if (!Path) return "/";
	//斜杠方向归一化
	Path = Path.replace('\\', '/').trim();
	//把重复的斜杠去掉
	while (Path != Path.replace('//', '/')) Path = Path.replace('//', '/');
	//必须以斜杠结尾
	//if (Path[Path.length - 1] != '/') Path = Path + "/";
	//必须以斜杠开头
	if (Path[0] != '/') Path = "/" + Path;
	return Path;
}

/**
 * 分割URL
 * @param {string} url 
 */
function SplitURL(url) {
	let nodes = url.split("/");
	//console.log(nodes);
	for (let i = nodes.length - 1; i >= 0; i--) {
		//console.log(i + ":" + nodes[i]);
		if (!nodes[i]) nodes.splice(i, 1);
		if (nodes[i] == "") nodes.splice(i, 1);
	}
	return nodes;
}

/**
 * 改变当前路径
 * @param {string} newpath 
 */
function ChangePath(newpath) {
	let path = FormatPath(newpath);
	$.post("api/FileManager/", JSON.stringify({
		Path: path
	}), function (result) {
		AllUnChoose();
		if (SplitURL(path).length > 0) ADDRESSBAR.canUp = true;
		let items = [];
		for (let i = 0; i < result.Files.length; i++) {
			let item = JSON.parse(JSON.stringify(result.Files[i]));
			item.Choose = false;
			item.Select = false;
			items.push(item);
		}

		DISH.Items = items;
		CANWRITE = result.CanWrite;
		CURRENTPATH = path;
		CookieManager.setCookie("CURRENTPATH", CURRENTPATH, null);
		ADDRESSBAR.path = path;
	});
}

/**
 * 必须阻止dragenter和dragover事件的默认行为，这样才能触发 drop 事件
 * @param {*} evt 
 */
function fileSelect(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	if (!CANWRITE) return;
	let files = evt.dataTransfer.files; //文件对象
	//处理多文件
	for (let i = 0, f; f = files[i]; i++) {
		uploadEvent({
			success: function (result) {
				if (result && result.success) ChangePath(CURRENTPATH);
			},
			progress: function (data) {
				if (data && data * 1 > 0) {
					//console.log(data);
					//progressElem.innerText = data
				}
			}
		}, f);
		continue;
	}
}

/**
 * 下载文件
 * @param {string} url 
 * @param {string} filename 
 */
function DownloadFile(url, filename) {
	//window.open("http://www.jb51.net","","top=100,left=100,width=300,height=200");
	//window.open("api/FileManager/DownloadFile?Path=" + url,"","");

	let a = document.createElement('a');
	a.href = "api/FileManager/DownloadFile?Path=" + url;
	a.download = filename;
	a.click();
	return;
	// */
	//下面是blob下载方式,但是这个没显示下载进度...
	fetch("api/FileManager/DownloadFile?Path=" + url).then(res => res.blob().then(blob => {
		let a = document.createElement('a');
		let bloburl = window.URL.createObjectURL(blob);
		a.href = bloburl;
		a.download = filename;
		a.click();
		window.URL.revokeObjectURL(bloburl);
	}));
}

/**
 * 
 * @param {*} evt 
 */
function dragOver(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	evt.dataTransfer.dropEffect = 'copy';
}

/**
 * 获取元素位置(相对于页面)大小
 * @param {*} element
 */
function getElementRect(element) {
	let actualLeft = element.offsetLeft;
	let actualTop = element.offsetTop;
	let current = element.offsetParent;
	while (current !== null) {
		actualLeft += current.offsetLeft;
		actualTop += current.offsetTop;
		current = current.offsetParent;
	}
	let elementScrollLeft = 0;
	let elementScrollTop = 0;
	if (document.compatMode == "BackCompat") {
		elementScrollLeft = document.body.scrollLeft;
		elementScrollTop = document.body.scrollTop;
	} else {
		elementScrollLeft = document.documentElement.scrollLeft;
		elementScrollTop = document.documentElement.scrollTop;
	}
	let obj = {
		X: actualLeft - elementScrollLeft,
		Y: actualTop - elementScrollTop,
		W: element.clientWidth,
		H: element.clientHeight

	};
	obj.R = obj.X + obj.W;
	obj.B = obj.Y + obj.H;
	return obj;
}

/**
 * 取消选择
 */
function AllUnChoose() {
	CHOOSEITEMS = [];
	for (let i = 0; i < DISH.Items.length; i++) DISH.Items[i].Choose = false;
}

function AllUnSelect() {
	for (let i = 0; i < DISH.Items.length; i++) DISH.Items[i].Select = false;
}

/**
 * 选择一个对象
 */
function Select(item, state) {
	item.Select = state;
}

/**
 * 切换一个对象的选中状态
 * @param {*} item 
 * @param {Boolean} state 
 */
function Choose(item, state) {
	item.Choose = state;
	for (let i = CHOOSEITEMS.indexOf(item); i >= 0; i = CHOOSEITEMS.indexOf(item)) CHOOSEITEMS.splice(i, 1);
	if (item.Choose) CHOOSEITEMS.push(item);
}

/**
 * 判断两个元素是否重叠
 * @param {*} element1 
 * @param {*} element2 
 */
function IsOverlap(element1, element2) {
	let e1rect = getElementRect(element1);
	let e2rect = getElementRect(element2);
	//判断左上角是否在范围里
	if ((e1rect.X < e2rect.R) && (e1rect.X > e2rect.X) && (e1rect.Y < e2rect.B) && (e1rect.Y > e2rect.Y)) return true;
	//判断左下角是否在范围里
	if ((e1rect.X < e2rect.R) && (e1rect.X > e2rect.X) && (e1rect.B < e2rect.B) && (e1rect.B > e2rect.Y)) return true;
	//判断右上角是否在范围里
	if ((e1rect.R < e2rect.R) && (e1rect.R > e2rect.X) && (e1rect.Y < e2rect.B) && (e1rect.Y > e2rect.Y)) return true;
	//判断右下角是否在范围里
	if ((e1rect.R < e2rect.R) && (e1rect.R > e2rect.X) && (e1rect.B < e2rect.B) && (e1rect.B > e2rect.Y)) return true;

	//判断左上角是否在范围里
	if ((e2rect.X < e1rect.R) && (e2rect.X > e1rect.X) && (e2rect.Y < e1rect.B) && (e2rect.Y > e1rect.Y)) return true;
	//判断左下角是否在范围里
	if ((e2rect.X < e1rect.R) && (e2rect.X > e1rect.X) && (e2rect.B < e1rect.B) && (e2rect.B > e1rect.Y)) return true;
	//判断右上角是否在范围里
	if ((e2rect.R < e1rect.R) && (e2rect.R > e1rect.X) && (e2rect.Y < e1rect.B) && (e2rect.Y > e1rect.Y)) return true;
	//判断右下角是否在范围里
	if ((e2rect.R < e1rect.R) && (e2rect.R > e1rect.X) && (e2rect.B < e1rect.B) && (e2rect.B > e1rect.Y)) return true;
	return false;
}

/**
 * 把选择的对象变为选中的
 */
function SelectToChoose() {
	for (let i = 0; i < DISH.Items.length; i++) {
		if (!DISH.Items[i].Select) continue;
		Choose(DISH.Items[i], !DISH.Items[i].Choose);
		DISH.Items[i].Select = false;
	}
}

//页面加载完才执行的函数
//ready和onload的区别:ready只是网页加载完 onload包含图片等资源都加载完
window.onload = function () {
	//构建文件盘对象
	DISH = new Vue({
		el: '#dish',
		data: {
			Items: [],
		},
		methods: {
			/**
			 * 空白处按下鼠标
			 */
			mousedown: function (event) {
				if (event.path[0].id != "dish") return;
				DOWNPOSITION.X = event.pageX;
				DOWNPOSITION.Y = event.pageY;
				CURRENTPOSITION.X = event.pageX;
				CURRENTPOSITION.Y = event.pageY;
				VUE.SelectBox.Top = CURRENTPOSITION.Y < DOWNPOSITION.Y ? CURRENTPOSITION.Y : DOWNPOSITION.Y;
				VUE.SelectBox.Left = CURRENTPOSITION.X < DOWNPOSITION.X ? CURRENTPOSITION.X : DOWNPOSITION.X;
				VUE.SelectBox.Width = 0;
				VUE.SelectBox.Height = 0;
				//console.log(event);
				VUE.SelectBox.Visual = true;
				if (!event.ctrlKey) AllUnChoose();
				MENU.Close();
			},

			/**
			 * 空白处移动鼠标
			 */
			mousemove: function (event) {
				if ((event.buttons & 1) == 0) return;
				if (!VUE.SelectBox.Visual) return;
				CURRENTPOSITION.X = event.pageX;
				CURRENTPOSITION.Y = event.pageY;
				VUE.SelectBox.Width = Math.abs(CURRENTPOSITION.X - DOWNPOSITION.X);
				VUE.SelectBox.Height = Math.abs(CURRENTPOSITION.Y - DOWNPOSITION.Y);
				VUE.SelectBox.Top = CURRENTPOSITION.Y < DOWNPOSITION.Y ? CURRENTPOSITION.Y : DOWNPOSITION.Y;
				VUE.SelectBox.Left = CURRENTPOSITION.X < DOWNPOSITION.X ? CURRENTPOSITION.X : DOWNPOSITION.X;
				//AllUnSelect();
				//上面动了界面,这里等待下界面刷新再继续.
				setTimeout(() => {
					for (let i = 0; i < DISH.$el.children.length; i++) {
						DISH.Items[i].Select = IsOverlap(DISH.$el.children[i], VUE.$el.children["SelectBox"]);
					}
				}, 10);
			},

			/**
			 * 空白处松开鼠标
			 */
			mouseup: function (event) {
				VUE.SelectBox.Visual = false;
				SelectToChoose();
			},

			/**
			 * 对象_按下鼠标
			 */
			mouse_item_down: function (event, item) {
				if ((event.buttons & 1) == 0) return;
				MENU.Close();
				//event.stopPropagation();
				//event.preventDefault();

				if(event.ctrlKey)
				{
					Choose(item, !item.Choose);
					return;
				}
				if(!item.Choose)
				{
					AllUnChoose();
					Choose(item, true);
					return;
				}
			},

			/**
			 * 对象_移动鼠标
			 */
			mouse_item_move: function (event, item) {},

			/**
			 * 对象_松开鼠标
			 */
			mouse_item_up: function (event, item) {
				if (event.which !== 1) return;
				if (VUE.SelectBox.Visual) return;
				if (event.ctrlKey) return;
				AllUnChoose();
				Choose(item, !item.Choose);
			},
			//双击项目
			dbclick_item: function (id, item) {
				MENU.Close();
				let clickpath = CURRENTPATH + item.name + "/";
				if (item.type == 1 || item.type == 2) ChangePath(clickpath);
				AllUnChoose();
			},
			//项目菜单
			click_item_right(event, id, item) {
				if (!item.Choose) {
					AllUnChoose();
					Choose(item, true);
				}
				this.OpenMenu(event);
				//阻止事件继续冒泡到上级div  已经在pug里写了,所以这里不用了
				//event.stopPropagation();
				//event.preventDefault();
			},
			//项目菜单+CTRL
			click_item_right_ctrl(event, id, item) {
				this.OpenMenu(event);
			},
			//空白区菜单
			click_bg_right(event) {
				AllUnChoose();
				this.OpenMenu(event);
			},
			//空白区菜单+CTRL
			click_bg_right_ctrl() {
				this.OpenMenu(event);
			},
			OpenMenu(event) {
				MENU.ShowDialog(null, event.pageX, event.pageY);
				MENU.canDownload = CHOOSEITEMS.length > 0;
				MENU.canDelete = CHOOSEITEMS.length > 0;
				MENU.canReName = CHOOSEITEMS.length == 1;
				MENU.canCut = CHOOSEITEMS.length > 0;
				MENU.canCopy = CHOOSEITEMS.length > 0;
				MENU.canPaste = PASTEMODE != -1 && CHOOSEITEMS.length == 0;
				MENU.canMkdir = CANWRITE && CHOOSEITEMS.length == 0;
			},
			//取消默认行为
			allowDrop(event) {
				event.preventDefault();
				//this.allowDrop(event);
				//console.log(event);
			},

			/**
			 * 
			 * @param {*} event 
			 * @param {*} item 
			 */
			dgstart(event, item) {
				//event.preventDefault();
				event.dataTransfer.setData("Text", item);
				event.dataTransfer.effectAllowed = "copy";
				//copy表示被拖动的数据将从当前位置复制到放置位置。
				//move表示被拖动的数据将被移动
				//link表示将在源和放置位置之间创建某种形式的关系或连接。
			},

			//取消默认行为
			dragend(event, item) {
				event.preventDefault();
				//this.allowDrop(event);
				//console.log(event);
			},
			drop(event, item) {
				//console.log(event);
				event.preventDefault();
				//this.allowDrop(event);
				let obj = event.dataTransfer.getData("Text");
				let target = item.allpath; //DISH.Items[event.path[1].id];
				if (item.type != 1) {
					alert("只能拖放到文件夹!");
					return;
				}
				/*
				console.log({
					Files: CHOOSEITEMS,
					To: target
				});// */

				let paths = [];
				for (let i = 0; i < CHOOSEITEMS.length; i++) {
					paths.push(CHOOSEITEMS[i].allpath);
				}
				$.post("api/FileManager/PastePaths", JSON.stringify({
					Paths: paths,
					Target: target,
					PasteMode: 0
				}), function (result) {
					ChangePath(CURRENTPATH);
					//ShowMessageBox("粘贴成功!");
				});
			}
		},
	});

	//构建组件对象
	VUE = new Vue({
		el: '#vue',
		methods: {
			editname_ok_click(obj) {
				//console.log(obj);
			},
			FindChildren(id) {
				for (let i = 0; i < this.$children.length; i++) {
					if (this.$children[i].$attrs.id == id) return this.$children[i];
				}
				return null;
			}
		},
		data: {
			SelectBox: {
				Visual: false,
				Top: 10,
				Left: 20,
				Width: 100,
				Height: 200
			},
			editvalue: "",
		}
	});
	//构建菜单对象
	MENU = new Vue({
		el: '#menu1',
		data: {
			//回调方法
			CallBack: null,
			//可见
			visual: false,
			canDownload: false,
			canDelete: false,
			canCut: false,
			canCopy: false,
			canPaste: false,
			canMkdir: false,
			canProperty: false,
			canReName: false,
			//位置
			Left: 0,
			Top: 0
		},
		methods: {
			//关闭全部
			CloseAll() {
				this.canReName = false;
				this.canDownload = false;
				this.canDelete = false;
				this.canCut = false;
				this.canCopy = false;
				this.canPaste = false;
				this.canMkdir = false;
				this.canProperty = false;
			},
			//删除
			click_Delete(event) {
				this.Close();
				let paths = [];
				for (let i = 0; i < CHOOSEITEMS.length; i++) {
					paths.push(CHOOSEITEMS[i].allpath);
				}
				$.post("api/FileManager/DeletePaths", JSON.stringify({
					Paths: paths
				}), function (jsonresult) {
					let result = JSON.parse(jsonresult);
					if (result && result.success) ChangePath(CURRENTPATH);
				});
				AllUnChoose();
			},
			//下载
			click_Download(event) {
				this.Close();
				for (let i = 0; i < CHOOSEITEMS.length; i++) {
					DownloadFile(CHOOSEITEMS[i].allpath, CHOOSEITEMS[i].name);
				}
				//ShowMessageBox("已经添加到下载队列,请耐心等候下载完成...");
			},
			//重命名
			click_ReName(event) {
				let item = CHOOSEITEMS[0];
				let en1 = VUE.FindChildren("en1");
				VUE.editvalue = item.name;
				en1.Title = "重命名";
				en1.Notice = "请在下方编辑文件名:"
				en1.ShowDialog((obj, iret) => {
					if (iret != 0) return;
					$.post("api/FileManager/Rename", JSON.stringify({
						Source: item.allpath,
						Target: FormatPath(CURRENTPATH) + VUE.editvalue
					}), function (result) {
						ChangePath(CURRENTPATH);
					});
				});
				AllUnChoose();
				this.Close();
			},
			//剪切
			click_Cut(event) {
				CUTORCOPYITEMS = JSON.parse(JSON.stringify(CHOOSEITEMS));
				PASTEMODE = 0;
				this.Close();
			},
			//拷贝
			click_Copy(event) {
				CUTORCOPYITEMS = JSON.parse(JSON.stringify(CHOOSEITEMS));
				PASTEMODE = 1;
				this.Close();
			},
			//粘贴
			click_Paste(event) {
				let paths = [];
				for (let i = 0; i < CUTORCOPYITEMS.length; i++) {
					paths.push(CUTORCOPYITEMS[i].allpath);
				}
				$.post("api/FileManager/PastePaths", JSON.stringify({
					Paths: paths,
					Target: CURRENTPATH,
					PasteMode: PASTEMODE
				}), function (result) {
					ChangePath(CURRENTPATH);
					//ShowMessageBox("粘贴成功!");
				});
				if (PASTEMODE == 0) PASTEMODE = -1;
				AllUnChoose();
				this.Close();
			},
			//建立文件夹
			click_Mkdir(event) {
				let en1 = VUE.FindChildren("en1");
				VUE.editvalue = "新文件夹";
				en1.Notice = "请输入文件夹名称:"
				en1.Title = "建立文件夹";
				en1.ShowDialog((obj, iret) => {
					if (iret != 0) return;
					$.post("api/FileManager/CreateFolder", JSON.stringify({
						Path: FormatPath(CURRENTPATH) + VUE.editvalue
					}), function (result) {
						ChangePath(CURRENTPATH);
					});
				});
				AllUnChoose();
				this.Close();
			},
			//关闭菜单
			Close() {
				this.visual = false;
				this.CallBack = null;
			},
			//显示模态窗口
			ShowDialog(callback, x, y) {
				MENU.CloseAll();
				MENU.Top = y;
				MENU.Left = x;
				this.visual = true;
				CallBack = callback;
			}
		} //End methods
	});

	//构建地址栏对象
	ADDRESSBAR = new Vue({
		el: '.AddressBar',
		methods: {
			UpClick(event) {
				let nodes = SplitURL(FormatPath(CURRENTPATH));
				//console.log(nodes);
				let newpath = "/";
				for (i = 0; i < nodes.length - 1; i++) newpath += nodes[i] + "/";
				//console.log(newpath);
				ChangePath(newpath);
			},
			ok(event) {
				ChangePath(this.path);
			}
		},
		data: {
			vprogress: false,
			canUp: false,
			path: CURRENTPATH,
			progress: 0
		}
	});

	//监听器
	var dropZone = document.getElementById('dish');
	dropZone.addEventListener('dragover', dragOver, false);
	dropZone.addEventListener('drop', fileSelect, false);
	CURRENTPATH = CookieManager.getCookie("CURRENTPATH");
	if (!CURRENTPATH) CURRENTPATH = "/";
	//初始目录
	ChangePath(CURRENTPATH);

	$(".AddressEdit").children("input").blur(() => {
		let input = $(".AddressEdit").children("input");
		input.hide();
	});
	$(".AddressEdit").children("div").click(() => {
		let input = $(".AddressEdit").children("input");
		input.show();
		//console.log(input);
		input.focus();
	});
};