import * as Router from 'koa-router';
import * as os from 'os';
//import * as db from "../DatabaseHandle";
import * as config from "../../config";
/*
import * as Public_Proc from "../Public_Proc";
import { Links, OyasumiPassword } from "../Entitys";
import { AddLinkResult, EncryptResult, DecryptResult } from "../WebApiStruct";
// */
const router = new Router();
router.prefix('/api/TaskManager/');

//计算内存使用率
function calcMem(): Object {
	let mem_total: number = os.totalmem(),
		mem_free = os.freemem(),
		mem_used = mem_total - mem_free,
		mem_ratio = 0;
	//let smem_total:string = (mem_total / (1024 * 1024 * 1024)).toFixed(1);
	//let smem_used:string = (mem_used / (1024 * 1024 * 1024)).toFixed(1);
	mem_ratio = Number(mem_used / mem_total * 100);
	mem_ratio -= mem_ratio % 1;
	return {
		total: mem_total,
		used: mem_used,
		ratio: mem_ratio
	}
}

router.use(async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
	let session=(<any>ctx).session;
	if (!("userinfo" in session))
	{
		ctx.body="";
		return;
	}
	await next();
});

router.get('/', async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
	ctx.body = {
		memory: calcMem(),
		cpu: { type: process.arch, info: os.cpus() },
		system: { platform: os.platform(), release: os.release() },
		networkinterface: os.networkInterfaces()
	};
});

export default router;