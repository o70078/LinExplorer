import * as diskinfo from "diskinfo"
import { Resolve, Reject } from "../../Public_Proc"
import * as Public_Proc from "../../Public_Proc";
import * as IconLib from "../../iconlib";
import * as Router from 'koa-router';
import * as os from 'os';
import * as fs from 'fs';
import * as path from 'path';
import * as Busboy from 'busboy';
import * as config from "../../config";
import * as uuid from 'node-uuid';
import { fileitem, paths, QueryDirResult } from "../../model";
import { setTimeout } from "timers";
import { WSAESTALE } from "constants";
import { PathLike } from "fs";
const router = new Router();
router.prefix('/api/FileManager/');
var GetDiskInfoDir_data: any = null;

async function Init() {
	if (!await exists("temp")) await mkdir("temp");
}

Init();

/**
 * 异步获取Windows系统的分区表
 * @return {promise<any>} 返回分区表对象数组
 */
async function GetDiskInfoDir(): Promise<any> {
	if (GetDiskInfoDir_data != null) return GetDiskInfoDir_data;
	GetDiskInfoDir_data = await new Promise<any>((resolve: Resolve<any>, reject: Reject) => {
		diskinfo.getDrives(
			function (err, aDrives) {
				if (err) {
					reject(err);
					return;
				}
				resolve(aDrives);
			});
	});
	return GetDiskInfoDir_data;
}

/**
 * 判断指定路径是否目录
 * @param {string} path
 */
async function isDirectory(path: string): Promise<boolean> {
	return await new Promise<boolean>((resolve: Resolve<boolean>, reject: Reject) => {
		fs.stat(path, function (err, stat) {
			if (err) {
				reject(err);
				return;
			}
			if (stat.isDirectory()) {
				resolve(true);
				return;
			}
			resolve(false);
			return;
		});
	});
}

/**
 * 判断指定路径是否文件
 * @param {string} path 
 */
async function isFile(path: string): Promise<boolean> {
	return await new Promise<boolean>((resolve: Resolve<boolean>, reject: Reject) => {
		fs.stat(path, function (err, stat) {
			if (err) {
				reject(err);
				return;
			}
			if (stat.isFile()) {
				resolve(true);
				return;
			}
			resolve(false);
			return;
		});
	});
}

/**
 * 判断一个路径是文件还是目录
 * @param {string}path 
 * @returns {number} -1:都不是 0:文件 1:目录
 */
async function IsFileOrDirectort(path: string): Promise<number> {
	return await new Promise<number>((resolve: Resolve<number>, reject: Reject) => {
		fs.stat(path, function (err, stat) {
			if (err) {
				reject(err);
				return;
			}
			if (stat.isFile()) {
				resolve(0);
				return;
			}
			if (stat.isDirectory()) {
				resolve(1);
				return;
			}
			resolve(-1);
			return;
		});
	});
}

/**
 * 测试一个路径是否可访问
 * @param {string} path 
 */
async function access(path: string): Promise<boolean> {
	return await new Promise<boolean>((resolve: Resolve<boolean>, reject: Reject) => {
		fs.access(path, function (err) {
			(err) ? resolve(false) : resolve(true);
			return;
		});
	});
}

/**
 * 测试一个路径是否存在
 * @param {string} path 
 */
async function exists(path: string): Promise<boolean> {
	return await new Promise<boolean>((resolve: Resolve<boolean>, reject: Reject) => {
		fs.exists(path, function (have: boolean) {
			resolve(have);
			return;
		});
	});
}

/**
 * 建立目录
 * @param {string} path 
 */
async function mkdir(path: string): Promise<boolean> {
	return await new Promise<boolean>((resolve: Resolve<boolean>, reject: Reject) => {
		fs.mkdir(path, function (err) {
			(err) ? resolve(false) : resolve(true);
			return;
		});
	});
}

/**
 * 读取一个目录下一级的文件/目录列表
 * @param {string} path 
 */
async function readdir(path: string): Promise<string[]> {
	return await new Promise<string[]>((resolve: Resolve<string[]>, reject: Reject) => {
		fs.readdir(path, function (err: NodeJS.ErrnoException, paths: string[]) {
			if (err) {
				reject(err);
				return;
			}
			resolve(paths);
		});
	});
}

/**
 * 复制文件
 * @param src 
 * @param dist 
 */
async function copyfile(src, dist): Promise<void> {
	return new Promise<void>((resolve: Resolve<void>, reject: Reject) => {
		fs.readFile(src, (err: NodeJS.ErrnoException, data: Buffer) => {
			if (err) {
				reject(err);
				return;
			}
			fs.writeFile(dist, data, (err: NodeJS.ErrnoException) => {
				if (err) {
					reject(err);
					return;
				}
				resolve();
			});
		});
	});
}

/**
 * 复制目录、子目录，及其中的文件
 * @param src {String} 要复制的目录
 * @param dist {String} 复制到目标目录
 */
async function copypath(src, dist): Promise<void> {
	//目录不存在时创建目录
	if (!(await access(dist))) await mkdir(dist);
	let paths: string[] = await readdir(src);
	for (let i = 0; i < paths.length; i++) {
		let path: string = paths[i];
		let _src = src + '/' + path;
		let _dist = dist + '/' + path;
		let stat: number = await IsFileOrDirectort(_src);
		//判断是文件还是目录
		if (stat == 0) {
			await copyfile(_src, _dist);
			return;
		}
		if (stat == 1) {
			//当是目录时，递归复制
			await copypath(_src, _dist);
			return;
		}
	};
}

/**
 * 移动文件/文件文件夹
 * @param {PathLike}src 源
 * @param {PathLike}dist 目标
 */
async function movepath(src: PathLike, dist: PathLike): Promise<void> {
	return await new Promise<void>((resolve: Resolve<void>, reject: Reject) => {
		fs.rename(src, dist, (err: NodeJS.ErrnoException) => {
			if (err) {
				reject(err);
				return;
			}
			resolve();
		});
	});
}

/**
 * 获取指定目录下所有文件目录列表
 * @param {string} root 目录
*/
function getAllFiles(root: string): fileitem[] {
	var res: fileitem[] = [], files: string[] = fs.readdirSync(root);
	files.forEach(function (file) {
		let info: fileitem = new fileitem();
		info.name = file;
		info.allpath = root + '/' + file;
		info.availabel = true;
		info.IconType = 3;
		info.icon = "images/file.svg";
		let stat: fs.Stats = null;
		try {
			stat = fs.lstatSync(info.allpath);
		}
		catch{
			info.type = -1;
			info.availabel = false;
			info.icon = "images/unknowfile.svg";
			res.push(info);
			return;
		}
		if (!stat.isDirectory()) {
			info.type = 0;
		} else {
			info.type = 1;
			info.icon = "images/folder.svg";
		}
		res.push(info);
	});
	return res
}

/**
 * 格式化路径
 * @param {string}path
 */
function FormatPath(path: string): string {
	//斜杠方向归一化
	let Path = path.replace('\\', '/').trim();
	//把重复的斜杠去掉
	while (Path != Path.replace('//', '/')) Path = Path.replace('//', '/');
	//必须不以斜杠结尾
	if (Path[Path.length - 1] == '/') Path = Path.substr(0,Path.length);
	//必须以斜杠开头
	if (Path[0] != '/') Path = "/" + Path;
	//如果是Windows系统,前面的斜杠也去掉
	if (Public_Proc.IsWindowsOS() && Path[0] == "/") Path = Path.substr(1);
	return Path;
}

/**
 * 分割URL
 * @param {string} url 
 */
function SplitURL(url: string): string[] {
	let nodes: string[] = FormatPath(url).split("/");
	for (let i = nodes.length - 1; i >= 0; i--) {
		if (!nodes[i]) nodes.splice(i, 1);
		if (nodes[i] == "") nodes.splice(i, 1);
	}
	return nodes;
}

//递归删除一个目录
let deleteFolderRecursive = function (path: string) {
	let files: string[] = [];
	if (!fs.existsSync(path)) return;
	if (!fs.statSync(path).isDirectory()) {
		fs.unlinkSync(path);
		return;
	}

	files = fs.readdirSync(path);
	for (let i = 0; i < files.length; i++) {
		deleteFolderRecursive(path + "/" + files[i]);
	};
	fs.rmdirSync(path);
}

/**
 * 查询指定目录的下级项目列表
 * @param {Router.IRouterContext} ctx
 * @param {()=>Promise<any>} next
 */
router.post('/', async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
	if (ctx.request.body) ctx.query = JSON.parse(ctx.request.body);
	let Path: string = '/';
	if ('Path' in ctx.query) Path = ctx.query.Path;

	ctx.body = new QueryDirResult();
	ctx.body.CanWrite = true;
	ctx.body.Code = 0;

	//获取目录下的文件/文件夹列表
	if (!Public_Proc.stringIsEmpty(Path)) {
		Path = FormatPath(Path);
		if (!Public_Proc.stringIsEmpty(Path)) {
			try {
				ctx.body.Files = getAllFiles(Path);
			}
			catch
			{
				ctx.body.Files = [];
			}
			return;
		}
	}
	if (!Public_Proc.IsWindowsOS()) {
		ctx.body.Files = getAllFiles("/");
		return;
	}

	//Windows系统独特情况,获取分区列表
	let partitions: any[] = await GetDiskInfoDir();
	let fileitems: fileitem[];
	fileitems = [];
	for (var i = 0, len = partitions.length; i < len; i++) {
		let partition: any = partitions[i];
		let info: fileitem = new fileitem();
		info.name = partition.mounted;
		info.allpath = Path + info.name;
		info.type = 2;
		info.IconType = 0;
		info.icon = IconLib.harddisk;
		fileitems.push(info);
	}
	ctx.body.Files = fileitems;
	ctx.body.CanWrite = false;
	return;
});

/**
 * 删除指定的一些路径
 * @param {Router.IRouterContext} ctx
 * @param {()=>Promise<any>} next
 */
router.post('/DeletePaths', async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
	let temp: any;
	if (ctx.request.body) temp = JSON.parse(ctx.request.body);
	//ctx.query = temp;
	//console.log(ctx.request.body);
	//console.log(ctx.query);


        ctx.body="测试版仅供参考!"
        return;

	if (!('Paths' in temp)) {
		ctx.body = JSON.stringify({
			success: false,
			message: "请提交要删除的文件/文件夹路径!"
		});
		return;
	}
	let Paths: string[] = temp.Paths;
	for (let i = 0; i < Paths.length; i++) {
		deleteFolderRecursive(Paths[i]);
	}
	ctx.body = JSON.stringify({
		success: true,
		message: "成功"
	});
});

/**
 * 创建目录
 * @param {Router.IRouterContext} ctx
 * @param {()=>Promise<any>} next
 */
router.post('/CreateFolder', async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
	let temp: any;
	if (ctx.request.body) temp = JSON.parse(ctx.request.body);

        ctx.body="测试版仅供参考!"
        return;

	if (!('Path' in temp)) {
		ctx.body = JSON.stringify({
			success: false,
			message: "请提交要建立的文件夹的路径!"
		});
		return;
	}
	let path = FormatPath(temp.Path);
	if (await exists(path)) {
		let i = 0;
		while (await exists(`${path}(${i})/`)) i++;
		path = `${path}(${i})/`;
	}
	await mkdir(path);
	ctx.body = JSON.stringify({
		success: true,
		message: "成功"
	});
});

/**
 * 粘贴文件/文件夹
 * @param {Router.IRouterContext} ctx
 * @param {()=>Promise<any>} next
 */
router.post('/PastePaths', async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
	let temp: any;
	if (ctx.request.body) temp = JSON.parse(ctx.request.body);
	
        ctx.body="测试版仅供参考!"
        return;

	if (!('Paths' in temp)) {
		ctx.body = JSON.stringify({
			success: false,
			message: "请提交要粘贴的文件/文件夹源路径!"
		});
		return;
	}
	if (!('Target' in temp)) {
		ctx.body = JSON.stringify({
			success: false,
			message: "请提交要粘贴的目标路径!"
		});
		return;
	}
	if (!('PasteMode' in temp)) {
		ctx.body = JSON.stringify({
			success: false,
			message: "请提交粘贴模式(剪切|复制)!"
		});
		return;
	}

	let paths: string[] = temp.Paths;
	let target: string = FormatPath(temp.Target);
	if(target[target.length-1]!='/')target+='/';
	//0:剪切 1:复制
	let PasteMode: number = temp.PasteMode;

	for (let i = 0; i < paths.length; i++) {
		let spilitpath: string[] = SplitURL(paths[i]);
		let objName = spilitpath[spilitpath.length - 1];
		let objtype = await IsFileOrDirectort(paths[i]);
		let objtarget = target + objName;
		//如果源和目标相同,则默默跳过
		if (paths[i] == objtarget) continue;
		if (PasteMode == 1) {
			if (objtype == 0) await copyfile(paths[i], objtarget);
			if (objtype == 1) await copypath(paths[i], objtarget)
		}
		if (PasteMode == 0) await movepath(paths[i], objtarget);
	}

	ctx.body = JSON.stringify({
		success: true,
		message: "成功"
	});
});

/**
 * 重命名文件/文件夹
 * @param {Router.IRouterContext} ctx
 * @param {()=>Promise<any>} next
 */
router.post('/Rename', async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
	let temp: any;
	if (ctx.request.body) temp = JSON.parse(ctx.request.body);
	
	ctx.body="测试版仅供参考!"
	return;

	if (!('Source' in temp)) {
		ctx.body = JSON.stringify({
			success: false,
			message: "请提交原路径!"
		});
		return;
	}

	if (!('Target' in temp)) {
		ctx.body = JSON.stringify({
			success: false,
			message: "请提交目标路径!"
		});
		return;
	}
	//如果源和目标相同,则默默跳过
	let source: string = FormatPath(temp.Source);
	let target: string = FormatPath(temp.Target);
	if (source != target) {
		await movepath(source, target);
	}

	ctx.body = JSON.stringify({
		success: true,
		message: "成功"
	});
});

/**
 * 上传文件
 * @param {Router.IRouterContext} ctx
 */
router.post('/UploadFile', async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
	//if (ctx.request.body) ctx.query = JSON.parse(ctx.request.body);
	let busboy = new Busboy({ headers: ctx.req.headers });

        ctx.body="测试版仅供参考!"
        return;

	let UpdateFileResult: any = await new Promise((resolve, reject) => {
		//console.log('文件上传中...');
		let result: any = {
			success: false,
			formData: {},
			message: "",
			tempfilename: uuid.v4(),
			filename: "",
			path: ""
		};

		//解析请求文件事件
		busboy.on('file', function (fieldname: string, file, filename: string, encoding, mimetype: string) {
			result.filename = filename;
			let saveTo = path.join("./temp/", result.tempfilename);
			//文件保存到指定路径
			file.pipe(fs.createWriteStream(saveTo));
			//文件写入事件结束
			file.on('end', function () {
				result.success = true;
				result.message = '文件上传成功';
				//console.log('文件上传成功!');
				resolve(result);
			});
		});

		//解析表单中其他字段信息
		busboy.on('field', function (fieldname: string, val: any, fieldnameTruncated, valTruncated, encoding, mimetype: string) {
			if (fieldname == "path") {
				result.path = val;
				if (Public_Proc.IsWindowsOS() && result.path[0] == "/") result.path = result.path.substr(1);
				return;
			}
			//console.log('表单字段数据[' + fieldname + ']:value: ' + val);
			result.formData[fieldname] = val;
		});

		//解析结束事件
		busboy.on('finish', function () {
			//console.log('文件上结束');
			resolve(result);
		});

		//解析错误事件
		busboy.on('error', function (err) {
			//console.log('文件上出错');
			reject(result);
		});
		ctx.req.pipe(busboy);
	});
	/*
	if (UpdateFileResult.success) {
		console.log("文件上传成功!");
	} else {
		console.log("文件上传失败!");
	}
	// */

	fs.rename("./temp/" + UpdateFileResult.tempfilename, UpdateFileResult.path + "/" + UpdateFileResult.filename, function (err: NodeJS.ErrnoException) { });
	ctx.body = JSON.stringify({
		success: UpdateFileResult.success,
		message: UpdateFileResult.message
	});

	//console.log(ctx);
});

/**
 * 下载文件
 * @param {Router.IRouterContext} ctx
 */
router.get('/DownloadFile', async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
	let temp: any;

        ctx.body="测试版仅供参考!"
        return;

	if (!('Path' in ctx.request.query)) {
		ctx.body = JSON.stringify({
			success: false,
			message: "请提交要下载的文件的路径!"
		});
		return;
	}
	let uri: string = ctx.request.query.Path;
	ctx.headers["Content-Disposition"] = "attachment;filename=" + '"' + uri.match("([^/]*?)(.[^./]*)?$")[0] + '"';
	ctx.body = fs.createReadStream(uri);
});

//header('Content-type:video/avi');
export default router;
