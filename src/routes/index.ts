import * as Router from 'koa-router';
import * as os from 'os';
//import * as db from "../DatabaseHandle";
import * as config from "../config";
/*import { Links, OyasumiPassword } from "../Entitys";
import { AddLinkResult, EncryptResult, DecryptResult } from "../WebApiStruct";
// */
const router = new Router();
router.prefix('/');

var temp: any = { title: "LinExplorer", landed: true };

router.use(async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
	temp = { title: "LinExplorer", landed: true };
	let session = (<any>ctx).session;
	if ("userinfo" in session) {
		temp.userinfo = session.userinfo;
	}
	else {
		temp.landed = false;
	}
	//console.log(ctx.session);
	await next();
});
// */
router.get('/Login', async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
	if (temp.landed) {
		ctx.status = 302;
		ctx.redirect("/");
		return;
	}
	temp.active = -1;
	await ctx.render('Login', temp);
});
class Respond_Base {
	//返回码
	code: number = 0;
	//提示消息
	msg: string = "";
}
class Respond_Login extends Respond_Base {

}
router.post('/Login', async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
	ctx.query = JSON.parse(ctx.request.body);
	if (ctx.query.username != config.SuperUser) {
		let res = new Respond_Login();
		res.code = -1;
		res.msg = "用户名错误!";
		ctx.body = res;
		return;
	}
	if (ctx.query.password != config.SuperUserPassword) {
		let res = new Respond_Login();
		res.code = -1;
		res.msg = "密码错误!";
		ctx.body = res;
		return;
	}
	(<any>ctx).session.userinfo = new Object();
	(<any>ctx).session.userinfo.username = ctx.query.username;
	(<any>ctx).session.userinfo.time = new Date();
	{
		let res = new Respond_Login();
		res.code = 0;
		res.msg = "登录成功!";
		ctx.body = res;
		return;
	}
});

router.use(async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
	if (!temp.landed) {
		ctx.status = 302;//301表示永久重定向 302是临时重定向
		ctx.headers["Cache-Control"] = "no-cache,no-store,must-revalidate";
		ctx.headers["Pragma"] = "no-store";
		ctx.headers["Expires"] = "0";
		ctx.redirect("/Login");
		return;
	}
	//console.log(ctx.session);
	await next();
});

router.get('/', async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
	temp.active = 0;
	temp.cpus = os.cpus();
	await ctx.render('index', temp);
});

router.get('/Explorer', async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
	temp.active = 1;
	await ctx.render('Explorer', temp);
});

router.get('/TaskManager', async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
        temp.active = 2;
        await ctx.render('TaskManager', temp);
})

router.get('/NetManager', async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
        temp.active = 3;
        await ctx.render('TaskManager', temp);
})

//application/octet-stream
export default router;
