//文件项目
export class fileitem {
	name: string;
	allpath: string;
	type: number;//0:file 1:folder 2:parttion -1:error
	availabel: boolean = true;//是否可访问,默认为true
	icon: string | paths[];
	IconType: number;//0:SVG 1:ImageFile 2:SVGPaths 3:SVGFiles
}

//查询目录内容返回的对象
export class QueryDirResult {
	Files: fileitem[];
	Code: number;
	CanWrite:Boolean;
}

export class paths {
	path: string;
	fill: string;
}

